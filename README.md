Верстка In Real Life
=============
Актуальная версия:
<https://inreallife.herokuapp.com/>

Base project on serve
=============

Learn more about serve:

<https://github.com/jlong/serve/>

How install and run project?
-------------------------------

1. Install ruby

        debian/ubuntu: apt-get install ruby

2. Install rubygems

    debian/ubuntu:
        apt-get install rubygems
        export PATH=/var/lib/gems/1.8/bin:$PATH

3. Install bundle:

        gem install bundle


4. Install gems:

        bundle install

5. Run serve:

        serve

6. http://localhost:3000